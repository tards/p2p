import java.net.*; 
import java.io.*;
import java.util.*;

public class Appli{
	public static void main(String args[]){

		try{
			Node node_Client;
			Console console = System.console();
			String ip = "";
			String prems ="";
			while(!prems.equals("o")&&!prems.equals("n")){
				prems = console.readLine("Existe-t-il déjà des pairs sur le réseau ? (o/n) ");
			}
			ip = InetAddress.getLocalHost().getHostAddress();
			if(prems.equals("o")){
				node_Client = new Node(ip.toString(),true);
			}else{
				node_Client = new Node(ip.toString(),false);
			}

			ServerSocket serverSocket = new ServerSocket(8000);
			Socket socket = serverSocket.accept();
			InputStream input = socket.getInputStream();
			OutputStream output = socket.getOutputStream();
			BufferedReader buffer = new BufferedReader(new InputStreamReader(input));
			PrintWriter printWriter = new PrintWriter(output,true);

			System.out.println(InetAddress.getLocalHost().getHostAddress());

			String message = "";
			while(true){
				message = buffer.readLine();
				System.out.println("Message reçu: " + message);
				(new Repondeur(node_Client,message)).start();
			}

		}catch(IOException e){
			System.out.println("IO");
		}
	}
}