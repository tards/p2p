import java.util.*;
import java.net.*;
import java.io.*;

public class Node {

	public static int MAX_ENTRY = 4;

	private String ip;
	private String id;
	private HashMap<String,String> fingers;
	private String predecessor;

	/**
	 * Constructeur de la classe Node
	 * @param ip L'IP du Node à creer
	 * @param firstNode true si c'est le premier pair du reseau, false sinon
	 */
	public Node(String ip, boolean firstNode){
		Console console = System.console();
		this.ip = ip;
		this.id = this.getIdFromIp(ip.toString());
		this.fingers = new HashMap<String,String>();
		System.out.println("Mon IP: "+this.ip);
		System.out.println("Mon ID: "+this.id);
		if (!firstNode)
		{
			this.fingers.put(this.id,this.ip);
			this.predecessor = this.id;
		}
		else
		{
			ip = console.readLine("IP parrain : ");
			this.fingers.put(this.getIdFromIp(ip),ip);
			this.init_finger_table();
			this.find_predecessor(this.id);
		}
		System.out.println("Prédecesseur: " + this.predecessor); 
		System.out.println("Table: " + this.fingers);
	}

	/**
	* Permet de générer un identifiant unique (ip sans les points) selon l'IP utilisateur
	* exemples :
	* - 192.168.30.124 devient "192168030124"
	* - 10.0.0.5 devient "010000000005"
	* @param ip L'IP utilisateur
	* @return une chaine de 12 caractères contenant l'identifiant unique
	*/
	public String getIdFromIp(String ip){

		String[] ip_parts = ip.split("\\.");
        String id = "";
        for (String s : ip_parts)
        {
        	if (s.length() == 1) // 5 devient 005
        		id += "00" + s;
        	else if (s.length() == 2) // 30 devient 030
        		id += "0" + s;
        	else if (s.length() == 3) // 124 reste 124
        		id += s;
        }
        return id;
	}

	/**
	* Permet de retrouver l'ip d'un utilisateur à  partir de son id
	* exemples :
	* - "192168030124" devient 192.168.30.124
	* - "010000000005" devient 10.0.0.5 
	* @param id L'id utilisateur
	* @return une chaine de caractères équivalente à  l'IP de l'utilisateur
	*/
	public String getIpFromId(String id){

		String[] ip_parts = id.split("(?<=\\G...)");
		String ip = "";
		for (String s : ip_parts)
		{
			while (s.startsWith("0"))
			{
				s = s.substring(1);
			}
			ip += s;
		}
		return ip;
	}
	

	/**
	* Permet à  un Node d'envoyer un message msg à  un membre en connaissant son ip. Le message est à  destination finale 
	* du membre désigné par son id
	* Envoi un message à  l'IP du membre passé en paramètre.
	* @param ip L'IP d'un membre présent sur le réseau
	* @param msg Le message à  envoyer
	*/
	public void sendMessage(String ip, String msg)
	{
		try
		{
			Socket sock = new Socket(ip,8000);

			OutputStream fluxSortie = sock.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(fluxSortie);
			BufferedWriter bw = new BufferedWriter(osw);
			bw.write(msg);

			sock.close();
		}
		catch (UnknownHostException e)
		{
			System.out.println("Node not found");
		}
		catch (IOException e)
		{
			System.out.println("IO Exception");
		}
	}

	public void find_successor(String idCaller)
	{
		// Récupère les id dans un arraylist
		ArrayList<String> keys = new ArrayList<String>();
		keys.addAll(this.fingers.keySet());

		// si le successeur du node actuel est différent du node max
		// et donc si le node actuel n'est pas le prédecesseur que l'on cherche
		if(!idCaller.equals(keys.get(0))) 
		{
			String closerNode = this.closest_preceding_finger(this.id,idCaller);
			sendMessage(closerNode,"find_successor@"+idCaller);
		}
		else // si le node actuel est le predecesseur que l'on cherche
		{
			sendMessage(idCaller,"rep_find_successor@"+this.getIdFromIp(this.fingers.get(1)));
		}
	}
	
	/**
	* On vérifie si celui qu'on cherche est le successeur du node courant
	* Si oui, c'est le prédecesseur
	* Sinon, on cherche un autre node le plus proche possible avant idMax
	*/
	public void find_predecessor(String idCaller)
	{
		// Récupère les id dans un arraylist
		ArrayList<String> keys = new ArrayList<String>();
		keys.addAll(this.fingers.keySet());

		// si le successeur du node actuel est différent du node max
		// et donc si le node actuel n'est pas le prédecesseur que l'on cherche
		if(!idCaller.equals(keys.get(0))) 
		{
			String closerNode = this.closest_preceding_finger(this.id,idCaller);
			sendMessage(closerNode,"find_pred@"+idCaller);
		}
		else // si le node actuel est le predecesseur que l'on cherche
		{
			sendMessage(idCaller,"rep_find_pred@"+this.id);
		}
	}


	/**
	* On cherche à taton le node le plus proche de idMax et qui le précède
	* Ce node est le plus près connu pour le lanceur de la fonction, mais pas forcément pour de vrai
	*/
	public String closest_preceding_finger(String idCaller, String idMax)
	{
		// Récupère les id dans un arraylist
		ArrayList<String> keys = new ArrayList<String>();
		keys.addAll(this.fingers.keySet());

		// pour chaque entrée de la table de routage du plus loin au plus proche du node effectuant l'opération
		for (int i=MAX_ENTRY;i<=0;i--)
		{
			// si : this.id < i < idMax
			if (keys.get(i).compareTo(idMax) < 0 && keys.get(i).compareTo(this.id) > 0) 
			{
				return keys.get(i);
				// sendMessage
			}
		}
		return this.id;
		// sendMessage
	}

	/**
	* Initialise sa propre table de routage. On envoit un message aux MAX_ENTRY pairs suivants qui sont situés à une distance
	* correspondant à une puissance de 2.
	*/
	public void init_finger_table()
	{
		int cpt_fingers = 0; // nombre d'entrees actuelles dans la table
		int cpt_nodes = 0; // compte le nombre de nodes parcourues
		boolean boucle = false; // a-t-on fait le tour du réseau ?
		while (cpt_fingers<MAX_ENTRY && !boucle)
		{
			if(cpt_nodes % Math.pow(2, cpt_fingers) == 0)
			{
				this.get_ith_successor(this.id,cpt_nodes,1); // on veut la ieme Node, on demande a la premiere node
				cpt_fingers++;
			}
			cpt_nodes++;
		}
	}

	/**
	* Envoi un message de successeur direct en successeur direct afin de connaitre 
	* le ième successeur d'un Node donné
	* @param idCaller L'ID du demandeur initial, c'est à  lui qu'il faut répondre à  la fin
	* @param i Précise le node dont on veut connaitre l'ID
	* @param cpt Précise ou on est rendus entre le demandeur et la cible
	*/
	public void get_ith_successor(String idCaller, int i, int cpt)
	{
		if (i==cpt) // si on est arrivé à la cible
		{	
			String caller_ip = getIpFromId(idCaller);
			String msg = "ith_successor_found@"+this.id;
			this.sendMessage(caller_ip,msg);
		}
		else // on doit relayer le message
		{
			String successor_ip = this.fingers.get(0);
			String msg = "get_ith_successor@"+/*caller*/idCaller+"@"+/*target*/i+"@"+/*counter*/(cpt++);
			this.sendMessage(successor_ip,msg);
		}
	}

	public void add_ith_successor(String id){
		this.fingers.put(id,this.getIpFromId(id));
		System.out.println("Nouvelle table:\n" + this.fingers);
	}
	
	/**
	 * Permet de fixer le predecesseur du Node courant
	 * @param idNewNode le nouveau predecesseur
	 */
	public void setPredecessor(String idNewNode)
	{
		this.predecessor = idNewNode;
	}
	
	/**
	 * Permet de fixer le successeur du Node courant
	 * @param idNewNode le nouveau successeur
	 */
	public void setSuccessor(String idNewNode)
	{
		// la nouvelle hashmap avec nouveau successeur direct
		HashMap<String,String> updatedFingers = new HashMap<String,String>();
		updatedFingers.put(idNewNode, this.getIpFromId(idNewNode));
		
		// le liste des clés
		ArrayList<String> keys = new ArrayList<String>();
		keys.addAll(this.fingers.keySet());
		
		// on ajoute les autres successeurs
		for (int i=1;i<keys.size();i++)
		{
			String id = keys.get(i);
			updatedFingers.put(id, this.getIpFromId(id));
		}
		
		// on enregistre la nouvelle hashmap
		this.fingers = updatedFingers;
	}
}