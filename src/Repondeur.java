import java.net.*;
import java.io.*;
import java.util.*;

public class Repondeur extends Thread {
	private String message;
	private Node node;

	public Repondeur(Node node, String message){
		this.node = node;
		this.message = message;
	}

	public void run(){
		String messageTab[] = this.message.split("@");
		String commande = messageTab[0];
		
		if(commande.equals("find_pred")){
			this.node.find_predecessor(messageTab[1]);
		}else if(commande.equals("rep_find_pred")){
			this.node.setPredecessor(messageTab[1]);
		}else if(commande.equals("ith_successor_found")){
			this.node.add_ith_successor(messageTab[1]);
		}else if(commande.equals("get_ith_successor")){
			this.node.get_ith_successor(messageTab[1],Integer.parseInt(messageTab[2]),Integer.parseInt(messageTab[3]));
		}else if(commande.equals("find_successor")){
			this.node.find_successor(messageTab[1]);
		}else if(commande.equals("rep_find_successor")){
			this.node.setSuccessor(messageTab[1]);
		}
	}
}